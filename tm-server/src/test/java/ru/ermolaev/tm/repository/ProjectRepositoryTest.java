package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.ermolaev.tm.TaskManagerApplication;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.entity.User;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TaskManagerApplication.class)
public class ProjectRepositoryTest {

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private IUserRepository userRepository;

    private final User userA = new User();

    private final User userB = new User();

    private final Project projectA = new Project();

    private final Project projectB = new Project();

    @Before
    @Transactional
    public void prepareData() {
        userA.setLogin("admin");
        userRepository.save(userA);
        userB.setLogin("test");
        userRepository.save(userB);

        projectA.setName("admin-project");
        projectA.setUser(userA);
        projectRepository.save(projectA);

        projectB.setName("test-project");
        projectB.setUser(userB);
        projectRepository.save(projectB);
    }

    @After
    @Transactional
    public void clear() {
        projectRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    @Transactional
    public void saveTest() {
        Assert.assertEquals(2, projectRepository.findAll().size());
        @NotNull final Project project = new Project();
        projectRepository.save(project);
        Assert.assertEquals(3, projectRepository.findAll().size());
        @Nullable final Project project1 = projectRepository.getOne(project.getId());
        Assert.assertNotNull(project1);
        Assert.assertEquals(project.getId(), project1.getId());
    }

    @Test
    public void findByUserIdAndIdTest() {
        @Nullable final Project project = projectRepository.findByUserIdAndId(userA.getId(), projectA.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), projectA.getId());
    }

    @Test
    public void findByUserIdAndNameTest() {
        @Nullable final Project project = projectRepository.findByUserIdAndName(userA.getId(), projectA.getName());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), projectA.getId());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, projectRepository.findAll().size());
    }

    @Test
    public void findAllByUserIdTest() {
        Assert.assertEquals(2, projectRepository.findAll().size());
        for (int i = 0; i < 5; i++) {
            Project project = new Project();
            project.setUser(userA);
            projectRepository.save(project);
        }
        for (int i = 0; i < 5; i++) {
            Project project = new Project();
            project.setUser(userB);
            projectRepository.save(project);
        }
        Assert.assertEquals(6, projectRepository.findAllByUserId(userA.getId()).size());
    }

    @Test
    @Transactional
    public void deleteTest() {
        Assert.assertEquals(2, projectRepository.findAll().size());
        projectRepository.delete(projectA);
        Assert.assertEquals(1, projectRepository.findAll().size());
        @Nullable final Project project = projectRepository.findById(projectA.getId()).orElse(null);
        Assert.assertNull(project);
    }

    @Test
    @Transactional
    public void deleteByIdTest() {
        Assert.assertEquals(2, projectRepository.findAll().size());
        projectRepository.deleteById(projectA.getId());
        Assert.assertEquals(1, projectRepository.findAll().size());
        @Nullable final Project project = projectRepository.findById(projectA.getId()).orElse(null);
        Assert.assertNull(project);
    }

    @Test
    @Transactional
    public void deleteByUserIdAndNameTest() {
        Assert.assertEquals(2, projectRepository.findAll().size());
        projectRepository.deleteByUserIdAndName(userA.getId(), projectA.getName());
        Assert.assertEquals(1, projectRepository.findAll().size());
        @Nullable final Project project = projectRepository.findById(projectA.getId()).orElse(null);
        Assert.assertNull(project);
    }

    @Test
    @Transactional
    public void deleteByUserIdAndIdTest() {
        Assert.assertEquals(2, projectRepository.findAll().size());
        projectRepository.deleteByUserIdAndId(userA.getId(), projectA.getId());
        Assert.assertEquals(1, projectRepository.findAll().size());
        @Nullable final Project project = projectRepository.findById(projectA.getId()).orElse(null);
        Assert.assertNull(project);
    }

    @Test
    @Transactional
    public void deleteAllTest() {
        Assert.assertEquals(2, projectRepository.findAll().size());
        projectRepository.deleteAll();
        Assert.assertEquals(0, projectRepository.findAll().size());
    }

    @Test
    @Transactional
    public void deleteAllByUserIdTest() {
        Assert.assertEquals(2, projectRepository.findAll().size());
        for (int i = 0; i < 5; i++) {
            Project project = new Project();
            project.setUser(userA);
            projectRepository.save(project);
        }
        for (int i = 0; i < 5; i++) {
            Project project = new Project();
            project.setUser(userB);
            projectRepository.save(project);
        }
        Assert.assertEquals(12, projectRepository.findAll().size());
        projectRepository.deleteAllByUserId(userA.getId());
        Assert.assertEquals(6, projectRepository.findAll().size());
    }

}
