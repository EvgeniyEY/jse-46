package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.ermolaev.tm.TaskManagerApplication;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.AuthUser;
import ru.ermolaev.tm.dto.ProjectDTO;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.enumeration.RoleType;

import java.util.ArrayList;
import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TaskManagerApplication.class)
public class ProjectServiceTest {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserService userService;

    private final static ProjectDTO preparedProjectA = new ProjectDTO();

    private final static ProjectDTO preparedProjectB = new ProjectDTO();

    private final static String admin = "admin";

    private final static String test = "test";

    private final AuthUser userA = new AuthUser(admin, admin, new ArrayList<>());

    private final AuthUser userB = new AuthUser(test, test, new ArrayList<>());

    private Project createdProjectA;

    private Project createdProjectB;

    @BeforeClass
    public static void prepare() {
        preparedProjectA.setName(admin);
        preparedProjectA.setDescription(admin);
        preparedProjectB.setName(test);
        preparedProjectB.setDescription(test);
    }

    @Before()
    public void prepareData() throws Exception {
        userA.setUserId(userService.create(admin, admin, RoleType.ADMINISTRATOR).getId());
        userB.setUserId(userService.create(test, test, RoleType.USER).getId());

        createdProjectA = projectService.createProject(userA.getUserId(), preparedProjectA);
        createdProjectB = projectService.createProject(userB.getUserId(), preparedProjectB);

        @NotNull final List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(admin, admin, authorities);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    public void clearData() {
        projectService.removeAll();
        userService.removeAll();
    }

    @Test
    public void updateByIdTest() throws Exception {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        @NotNull final String name = "newName";
        @NotNull final String description = "newDescription";
        projectDTO.setId(createdProjectA.getId());
        projectDTO.setName(name);
        projectDTO.setDescription(description);
        projectService.updateById(userA.getUserId(), projectDTO);
        @Nullable final Project project = projectService.findOneById(createdProjectA.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), createdProjectA.getId());
        Assert.assertEquals(project.getName(), name);
        Assert.assertEquals(project.getDescription(), description);
    }

    @Test
    public void countAllProjectsTest() {
        Assert.assertEquals(2, projectService.countAllProjects().intValue());
    }

    @Test
    public void countUserProjectsTest() throws Exception {
        Assert.assertEquals(1, projectService.countUserProjects(userA.getUserId()).intValue());
        Assert.assertEquals(1, projectService.countUserProjects(userB.getUserId()).intValue());
    }

    @Test
    public void findOneByIdTest() throws Exception {
        @Nullable final Project project = projectService.findOneById(createdProjectA.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), createdProjectA.getId());
    }

    @Test
    public void findOneByUserIdAndIdTest() throws Exception {
        @Nullable final Project project = projectService.findOneById(userB.getUserId(), createdProjectB.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), createdProjectB.getId());
    }

    @Test
    public void findOneByNameTest() throws Exception {
        @Nullable final Project project = projectService.findOneByName(userA.getUserId(), preparedProjectA.getName());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), createdProjectA.getId());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, projectService.findAll().size());
    }

    @Test
    public void findAllByUserIdTest() throws Exception {
        Assert.assertEquals(1, projectService.findAllByUserId(userA.getUserId()).size());
        Assert.assertEquals(1, projectService.findAllByUserId(userB.getUserId()).size());
    }

    @Test
    public void removeOneByIdTest() throws Exception {
        Assert.assertEquals(2, projectService.findAll().size());
        projectService.removeOneById(createdProjectA.getId());
        Assert.assertEquals(1, projectService.findAll().size());
        @Nullable final Project project = projectService.findOneById(createdProjectA.getId());
        Assert.assertNull(project);
    }

    @Test
    public void removeOneByUserIdAndIdTest() throws Exception {
        Assert.assertEquals(2, projectService.findAll().size());
        projectService.removeOneById(userA.getUserId(), createdProjectA.getId());
        Assert.assertEquals(1, projectService.findAll().size());
        @Nullable final Project project = projectService.findOneById(createdProjectA.getId());
        Assert.assertNull(project);
    }

    @Test
    public void removeOneByNameTest() throws Exception {
        Assert.assertEquals(2, projectService.findAll().size());
        projectService.removeOneByName(userA.getUserId(), createdProjectA.getName());
        Assert.assertEquals(1, projectService.findAll().size());
        @Nullable final Project project = projectService.findOneById(createdProjectA.getId());
        Assert.assertNull(project);
    }

    @Test
    public void removeAllTest() {
        Assert.assertEquals(2, projectService.findAll().size());
        projectService.removeAll();
        Assert.assertEquals(0, projectService.findAll().size());
    }

    @Test
    public void removeAllByUserIdTest() throws Exception {
        Assert.assertEquals(2, projectService.findAll().size());
        projectService.removeAllByUserId(userA.getUserId());
        Assert.assertEquals(1, projectService.findAll().size());
    }

}
