package ru.ermolaev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_project")
public class Project extends AbstractEntity {

    @Column(nullable = false)
    private String name = "";

    @Column
    private String description = "";

    private Date startDate;

    private Date completeDate;

    @Column(updatable = false)
    private Date creationDate = new Date(System.currentTimeMillis());

    @ManyToOne
    private User user;

    @OneToMany(
            mappedBy = "project",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList<>();

}
