package ru.ermolaev.tm.endpoint.view;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.AuthUser;
import ru.ermolaev.tm.dto.UserDTO;
import ru.ermolaev.tm.entity.User;

@Controller
@RequestMapping("/users")
public class UserController {

    private final IUserService userService;

    @Autowired
    public UserController(
            @NotNull final IUserService userService
    ) {
        this.userService = userService;
    }

    @GetMapping("/registration")
    public ModelAndView create() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/view/user/registration");
        return modelAndView;
    }

    @PostMapping("/registration")
    public ModelAndView create(
            @ModelAttribute("user") @NotNull final UserDTO userDTO
    ) throws Exception {
        userService.create(userDTO.getLogin(), userDTO.getPasswordHash(), userDTO.getEmail());
        return new ModelAndView("redirect:/");
    }

    @GetMapping("/info")
    public ModelAndView info(@AuthenticationPrincipal final AuthUser authUser) throws Exception {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/view/user/userInfo");
        @Nullable final User user = userService.findOneByLogin(authUser.getUsername());
        modelAndView.addObject("user", user);
        return modelAndView;
    }

}
