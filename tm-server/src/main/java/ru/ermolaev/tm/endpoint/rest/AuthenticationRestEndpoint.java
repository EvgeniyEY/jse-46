package ru.ermolaev.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/rest/authentication")
public class AuthenticationRestEndpoint {

    private final AuthenticationManager authenticationManager;

    public AuthenticationRestEndpoint(
            @NotNull final AuthenticationManager authenticationManager
    ) {
        this.authenticationManager = authenticationManager;
    }

    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return true;
        } catch (final Exception e) {
            return false;
        }
    }

    @GetMapping(value = "/logout")
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

}
