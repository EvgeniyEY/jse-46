package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.UserDTO;
import ru.ermolaev.tm.entity.Role;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.enumeration.RoleType;
import ru.ermolaev.tm.exception.empty.*;
import ru.ermolaev.tm.repository.IUserRepository;

import java.util.Collections;
import java.util.List;

@Service
public class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(
            @NotNull final IUserRepository userRepository,
            @NotNull final PasswordEncoder passwordEncoder
    ) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Nullable
    @Override
    public User getOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = userRepository.getOne(id);
        return user;
    }

    @Nullable
    @Override
    public User findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = userRepository.findById(id).orElse(null);
        return user;
    }

    @Nullable
    @Override
    public User findOneByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        return user;
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public List<UserDTO> findAll() {
        @NotNull final List<User> users = userRepository.findAll();
        return UserDTO.toDTO(users);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void removeOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void removeOneByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.deleteByLogin(login);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void removeAll() {
        userRepository.deleteAll();
    }

    @Override
    @Nullable
    @Transactional
    public User create(@Nullable final UserDTO userDTO) throws Exception {
        if (userDTO == null) return null;
        if (userDTO.getLogin() == null || userDTO.getLogin().isEmpty()) throw new EmptyLoginException();
        @NotNull final String login = userDTO.getLogin();
        if (userDTO.getPasswordHash() == null || userDTO.getPasswordHash().isEmpty()) throw new EmptyPasswordException();
        @NotNull final String password = userDTO.getPasswordHash();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(password));
        if (userDTO.getEmail() != null && !userDTO.getEmail().isEmpty()) user.setEmail(userDTO.getEmail());
        if (!userDTO.getRoles().isEmpty()) {
            @NotNull final Role role = new Role();
            role.setUser(user);
            role.setRoleType(userDTO.getRoles().get(0));
            user.setRoles(Collections.singletonList(role));
        }
        if (userDTO.getFirstName() != null && !userDTO.getFirstName().isEmpty()) user.setFirstName(userDTO.getFirstName());
        if (userDTO.getMiddleName() != null && !userDTO.getMiddleName().isEmpty()) user.setMiddleName(userDTO.getMiddleName());
        if (userDTO.getLastName() != null && !userDTO.getLastName().isEmpty()) user.setLastName(userDTO.getLastName());
        userRepository.save(user);
        return user;
    }

    @Override
    @NotNull
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setLogin(login);
        @NotNull final String encodedPass = passwordEncoder.encode(password);
        user.setPasswordHash(encodedPass);
        userRepository.save(user);
        return user;
    }

    @Override
    @NotNull
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        @NotNull final String encodedPass = passwordEncoder.encode(password);
        user.setPasswordHash(encodedPass);
        @NotNull final Role role = new Role();
        role.setUser(user);
        role.setRoleType(RoleType.USER);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
        return user;
    }

    @Override
    @NotNull
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (roleType == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setLogin(login);
        @NotNull final String encodedPass = passwordEncoder.encode(password);
        user.setPasswordHash(encodedPass);
        @NotNull final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
        return user;
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public Long count() {
        @NotNull final Long count = userRepository.count();
        return count;
    }

    @Override
    @Transactional
    public void updatePassword(
            @Nullable final String userId,
            @Nullable final String newPassword
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyPasswordException();
        @NotNull final String encodedPass = passwordEncoder.encode(newPassword);
        @Nullable final User user = findOneById(userId);
        if (user == null) return;
        user.setPasswordHash(encodedPass);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void updateUserFirstName(
            @Nullable final String userId,
            @Nullable final String newFirstName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newFirstName == null || newFirstName.isEmpty()) throw new EmptyFirstNameException();
        @Nullable final User user = findOneById(userId);
        if (user == null) return;
        user.setFirstName(newFirstName);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void updateUserMiddleName(
            @Nullable final String userId,
            @Nullable final String newMiddleName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newMiddleName == null || newMiddleName.isEmpty()) throw new EmptyMiddleNameException();
        @Nullable final User user = findOneById(userId);
        if (user == null) return;
        user.setMiddleName(newMiddleName);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void updateUserLastName(
            @Nullable final String userId,
            @Nullable final String newLastName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newLastName == null || newLastName.isEmpty()) throw new EmptyLastNameException();
        @Nullable final User user = findOneById(userId);
        if (user == null) return;
        user.setLastName(newLastName);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void updateUserEmail(
            @Nullable final String userId,
            @Nullable final String newEmail
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newEmail == null || newEmail.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = findOneById(userId);
        if (user == null) return;
        user.setEmail(newEmail);
        userRepository.save(user);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findOneByLogin(login);
        if (user == null) return;
        user.setLocked(true);
        userRepository.save(user);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findOneByLogin(login);
        if (user == null) return;
        user.setLocked(false);
        userRepository.save(user);
    }

}
