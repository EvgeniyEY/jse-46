
# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**login** | **String** |  |  [optional]
**passwordHash** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**firstName** | **String** |  |  [optional]
**middleName** | **String** |  |  [optional]
**lastName** | **String** |  |  [optional]
**role** | [**RoleEnum**](#RoleEnum) |  |  [optional]
**locked** | **Boolean** |  |  [optional]
**projects** | [**List&lt;Project&gt;**](Project.md) |  |  [optional]
**tasks** | [**List&lt;Task&gt;**](Task.md) |  |  [optional]
**sessions** | [**List&lt;Session&gt;**](Session.md) |  |  [optional]


<a name="RoleEnum"></a>
## Enum: RoleEnum
Name | Value
---- | -----
USER | &quot;USER&quot;
ADMIN | &quot;ADMIN&quot;



