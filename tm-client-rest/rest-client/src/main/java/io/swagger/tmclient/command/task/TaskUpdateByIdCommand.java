package io.swagger.tmclient.command.task;

import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.TaskDTO;
import io.swagger.tmclient.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import io.swagger.tmclient.util.TerminalUtil;

@Component
public class TaskUpdateByIdCommand extends AbstractCommand {

    @Autowired
    public TaskUpdateByIdCommand(@NotNull final DefaultApi api) {
        super(api);
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-update-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER TASK ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NEW TASK NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW TASK DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(id);
        taskDTO.setName(name);
        taskDTO.setDescription(description);
        api.updateById_0(taskDTO);
        System.out.println("[COMPLETE]");
    }

}
