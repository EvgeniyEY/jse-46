package io.swagger.tmclient.command.user;

import io.swagger.client.api.DefaultApi;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import io.swagger.tmclient.command.AbstractCommand;
import io.swagger.tmclient.util.TerminalUtil;

@Component
public class UserLoginCommand extends AbstractCommand {

    @Autowired
    public UserLoginCommand(@NotNull final DefaultApi api) {
        super(api);
    }

    @NotNull
    @Override
    public String commandName() {
        return "login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Login in your account.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        api.login(login, password);
        System.out.println("[OK]");
    }

}
