package io.swagger.tmclient.config;

import io.swagger.client.api.DefaultApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "io.swagger.tmclient")
public class ClientConfiguration {

    @Bean
    public DefaultApi api () {
        return new DefaultApi();
    }

}