package io.swagger.tmclient.util;

import org.jetbrains.annotations.Nullable;
import io.swagger.tmclient.exception.IncorrectIndexException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    @Nullable
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @Nullable
    static Integer nextNumber() throws Exception {
        @Nullable final String value = nextLine();
        if (value == null) return null;
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IncorrectIndexException(value);
        }
    }

}
