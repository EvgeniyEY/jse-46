package io.swagger.tmclient.command.user;

import io.swagger.client.api.DefaultApi;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import io.swagger.tmclient.command.AbstractCommand;

@Component
public class UserLogoutCommand extends AbstractCommand {

    @Autowired
    public UserLogoutCommand(@NotNull final DefaultApi api) {
        super(api);
    }

    @NotNull
    @Override
    public String commandName() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout from your account.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGOUT]");
        api.logout();
        System.out.println("[OK]");
    }

}
